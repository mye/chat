import socket


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.bind(('127.0.0.1', 10101))
        clients = []
        print("[ Server Started ]")

        while True:
            try:
                data, addr = sock.recvfrom(1024)

                if addr not in clients:
                    clients.append(addr)

                print(data.decode("utf-8"))

                for client in clients:
                    if addr != client:
                        sock.sendto(data, client)
            except:
                print("\n[ Server Stopped ]")
                break


if __name__ == '__main__':
    main()
