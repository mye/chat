import socket
import threading
import time


def receving(name, sock):
    while True:
        try:
            while True:
                data, addr = sock.recvfrom(1024)
                print(data.decode("utf-8"))
                time.sleep(0.2)

                if not data:
                    break
        except:
            pass


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.bind((socket.gethostbyname(socket.gethostname()), 0))

        server = ("127.0.0.1", 10101)
        sock.setblocking(0)

        rT = threading.Thread(target=receving, args=("RecvThread", sock))
        rT.start()
        sock.sendto(("user_2  <= join chat ").encode("utf-8"), server)

        while True:
            try:
                message = input()
                if message != "":
                    sock.sendto(("user_2 :: " + message).encode("utf-8"), server)
                time.sleep(0.2)
            except:
                sock.sendto(("user_2 => left chat ").encode("utf-8"), server)
                break
        rT.join()


if __name__ == '__main__':
    main()



